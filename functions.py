import time
global endPosition
endPosition = 0
OUT = open("./scanmemOUT")
IN = open("./scanmemIN", 'a')

def runScanmemCMD(cmd):
  IN = open("./scanmemIN", 'a')
  IN.write(cmd + "\n")
  IN.close()
  
def getNewOutput():
  time.sleep(.001)
  global endPosition
  OUT.seek(endPosition)
  data = ""
  while( True ):
    line = OUT.readline()
    data = data + line
    newPos = OUT.tell()
    if newPos == endPosition:
      break
    else:
      endPosition = newPos
  return data
